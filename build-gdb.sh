#!/bin/sh
set -ex
PROJECT=gdb
VERSION=8.2.1
ORIGDIR=$PROJECT-$VERSION
WORKDIR=$ORIGDIR.work

[ -d $ORIGDIR ] || apt source $PROJECT
rm -rf $WORKDIR
cp -r $ORIGDIR $WORKDIR
cd $WORKDIR

if [ -z "$VERSION_HEADER" ] ; then
	echo VERSION_HEADER is not defined
	exit 1
fi

STATIC_LIBS=/tmp/static-libs/lib
SOURCE_DIR=/usr/lib/x86_64-linux-gnu
rm -rf $STATIC_LIBS
mkdir -p $STATIC_LIBS
cp \
	$SOURCE_DIR/libisl.a \
	$SOURCE_DIR/libmpc.a  \
	$SOURCE_DIR/libmpfr.a \
	$SOURCE_DIR/libgmp.a  \
	$SOURCE_DIR/libbabeltrace.a  \
	$SOURCE_DIR/libbabeltrace-ctf.a  \
	$SOURCE_DIR/libncursesw.a  \
	$SOURCE_DIR/libtinfo.a  \
	$SOURCE_DIR/libpython2.7.a  \
	$STATIC_LIBS

SEDOUT="-i debian/rules"
# Find any append to the configure arguments and insert our mixture
sed -e "s|--with-babeltrace|--without-babeltrace|" $SEDOUT
sed -e "s|--with-intel-pt|--without-intel-pt|" $SEDOUT
sed -e "s|--with-lzma|--without-lzma|" $SEDOUT
sed -e "s|--with-mpfr|--without-mpfr|" $SEDOUT
sed -e "s|--enable-tui|--disable-tui|" $SEDOUT
sed -e "s|--with-python=python3|--with-python=python2|" $SEDOUT
sed -e "s|tinfo||" $SEDOUT
#only one occurence of with-expat
sed -e "s|--with-expat|--with-expat=no --without-system-readline --disable-tui --enable-static --disable-shared --without-mpfr|" $SEDOUT

sed -e "0,/^CPPFLAGS += -fPIC/s||\
CFLAGS += -include $VERSION_HEADER\n&|" $SEDOUT

ac_cv_search_tgetent=no DEB_BUILD_OPTIONS=nocheck dpkg-buildpackage -b -us -uc

