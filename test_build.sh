#!/bin/sh
set -xe

ARCH=$1
TRIPLET=$2
TOOLCHAIN=apertis-$TRIPLET-toolchain

cd $(dirname $0)

if [ ! -d $TOOLCHAIN ] ; then
	echo "Extracting $TOOLCHAIN.tar.xz"
	apt update
	apt install -y xz-utils
	tar xf $TOOLCHAIN.tar.xz
else
	echo "Existing $TOOLCHAIN.tar.xz found"
fi

cat > main.c <<EOF
#include <stdio.h>
void main() {
	printf("Hello world!\n");
}
EOF

export PATH=$TOOLCHAIN/usr/bin:$PATH

CC=$TRIPLET-gcc-8
WHICH=`which $CC`
if [ ! "$WHICH" = "$TOOLCHAIN/usr/bin/$CC" ]; then
   echo "Conflicting toolchain in Path: $WHICH != $TOOLCHAIN/usr/bin/$CC"
   exit 1
fi

echo "$TRIPLET-gcc main.c -c -o main"

rm -f main
$CC main.c -o main
chmod a+x main
exit 0

if ./main ; then
	echo "Passed"
else
	echo "Failed"
	exit 1
fi

