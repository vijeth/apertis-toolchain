#!/bin/bash
set -xe


WORK=$(pwd)/work
TEST=$(pwd)/test/
mkdir -p $TEST
CNAME=toolchain-tester-container
docker rm $CNAME || true

RUNINTESTER="docker run --rm --name $CNAME -v $TEST:/test -w /test"

cat <<EOF |
armhf arm-linux-gnueabihf
arm64 aarch64-linux-gnu
EOF
while read ARCH TRIPLET; do
	echo "$ARCH $TRIPLET"
	sudo rm -rf $TEST/*
	cp test_build.sh $TEST
	cp $WORK/apertis-$TRIPLET-toolchain.tar.xz $TEST
	if ! $RUNINTESTER ubuntu:16.04 ./test_build.sh $ARCH $TRIPLET ; then
		echo "***** FAILED *****"
		echo "Test $ARCH $TRIPLET failed"
		echo "Inspect with: $RUNINTESTER -ti toolchain-tester bash"
		echo "***** FAILED *****"
		exit 1
	fi
done

echo "Inspect with: $RUNINTESTER -ti toolchain-tester bash"
echo "PASSED"
