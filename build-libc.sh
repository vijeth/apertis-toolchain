#!/bin/sh
set -ex
PROJECT=cross-toolchain-base
VERSION=32
ORIGDIR=$PROJECT-$VERSION
WORKDIR=$ORIGDIR.work

ARCH=${1-armhf}
TRIPLET=${2-arm-linux-gnueabihf}

[ -d $ORIGDIR ] || apt source $PROJECT
rm -rf $WORKDIR
cp -r $ORIGDIR $WORKDIR
cd $WORKDIR

if [ -z "$VERSION_HEADER" ] ; then
	echo VERSION_HEADER is not defined
	exit 1
fi

SEDOUT="-i debian/rules"
# Dirty hack for linux-source-4.19
sed -e "s|zcat .*\\\\|echo Apertis toolchain\\\\|" $SEDOUT
#sed -e "s|CFLAGS.*-O2|& -include $VERSION_HEADER|"             $SEDOUT
#sed -e "s/enable-shared/disable-shared/"                       $SEDOUT
#sed -e "s|mv \$\$d_src/\$\$so.*|echo 'disabled[&]';\\\\|"        $SEDOUT
#sed -e "s|ln -sf \.\./\.\./\.\./lib/.*\.so.*|echo 'disabled[&]';\\\\|" $SEDOUT
#sed -e "s|mv \$\$d_src/\$\$lib\.a.*|echo 'disabled[&]';\\\\|"        $SEDOUT
#sed -e "s|ln -sf \.\./\.\./\.\./lib/.*\.a.*|echo 'disabled[&]';\\\\|" $SEDOUT
#sed -e "s/chmod ugo-x \$(D_CROSS).*/echo 'disabled[&]'/"                     $SEDOUT

CROSS_ARCHS=$ARCH dpkg-buildpackage -d -b -us -uc -j$(nproc)
